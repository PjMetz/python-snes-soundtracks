import tweepy
import config
import random

print("PLAYER 1 HAS JOINED")
#Schedule is mon-Fri at 6:30 am ETC

auth = tweepy.OAuth1UserHandler(config.consumer_key, config.consumer_secret, config.access_token, config.access_token_secret
)

api_m = tweepy.API(auth) 

bot_id = 1381651279945039872

soundtrack_list = ["https://www.youtube.com/watch?v=jV7YxfwT4r0", "https://www.youtube.com/watch?v=-QsysJwzod4", "https://youtu.be/e8tg8G2iD3U", "https://www.youtube.com/watch?v=tBWl9p0fH28&", "https://www.youtube.com/watch?v=oRxgYC5zrV4", "https://youtu.be/c68MrLSXcZ4", "https://youtu.be/85u34SUh05Y", "https://www.youtube.com/watch?v=wpchBo75N68", "https://www.youtube.com/watch?v=_4EjGXRDOH0", "https://www.youtube.com/watch?v=438nMbWncRU", "https://www.youtube.com/watch?v=UyNufyV3VCo", "https://www.youtube.com/watch?v=rJJk9Zk2h_U", "https://www.youtube.com/watch?v=A4YeAdyYq4M", "https://www.youtube.com/watch?v=_-XNMr3x0l4", "https://www.youtube.com/watch?v=sySkD2Nfwok", "https://www.youtube.com/watch?v=jUbU5tHfyCs", "https://www.youtube.com/watch?v=dIKdZ2827rM",];
soundtrack_list_length = len(soundtrack_list)

def player1(twt):
    the_id = twt.id
    name = twt.author.screen_name
    i = random.randint(0, soundtrack_list_length)
    message = f"@{name} Here's your soundtrack! 🎮 {soundtrack_list[i]}"

    print("Tweet Found!")
        #twt.author.screen_name, etc, are taken from Twitter docs LINK HERE
    print(f"TWEET: {name} - {twt.text}")
    if ((twt.author.id != bot_id) and not (hasattr(twt, "quoted_status")) and not (hasattr(twt, "retweeted_status"))):
        try:
            the_tweet = twt.extended_tweet["full_text"]
        except AttributeError:
            the_tweet = twt.text
        #tweet has "impress" and "shania's name in it
        if ("please" in the_tweet) and ("@SnesSoundtracks" in the_tweet):
            try:
                print("Attempting Comment")
                api_m.update_status(status = message, in_reply_to_status_id = the_id)
                print("TWEET SUCCESSFUL")
            except Exception as err:
                print(err)
        else:
            print("GAME OVER: Be Nicer")
    else:
        print("GAME OVER: Possible Doppleganger Detected")
class MyStreamListener(tweepy.Stream):
#on_status is what happens when a status ("tweet") comes into the stream
    def on_status(self, twts):
        player1(twts)               

         
#create instance
stream_m = MyStreamListener(config.consumer_key, config.consumer_secret, config.access_token, config.access_token_secret)
#use "follow" for RT a specific account, "track" for tweets that contain the word
stream_m.filter(track =["@SnesSoundtracks"])

